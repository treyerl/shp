import java.io.File;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;


public class Writer extends SHP{
	public Writer(File f) {
		super(f);
	}

	protected byte[] writeIntLittleEndian(int i){
		ByteBuffer bb = ByteBuffer.allocate(4).putInt(i);
		bb.order(ByteOrder.LITTLE_ENDIAN);
		return bb.array();
	}

	protected byte[] writeIntBigEndian(int i){
		byte[] bytes = new byte[4];
		ByteBuffer.wrap(bytes).putInt(i);
		return bytes;
	}
	
	protected byte[] writeDoubleLittleEndian(double d){
		ByteBuffer bb = ByteBuffer.allocate(8).putDouble(d);
		bb.order(ByteOrder.LITTLE_ENDIAN);
		return bb.array();
	}
	
	protected byte[] writeBBoxLittleEndian(double[] d){
		ByteBuffer bb = ByteBuffer.allocate(32);
		bb.putDouble(0, d[0]);
		bb.putDouble(8, d[1]);
		bb.putDouble(16, d[2]);
		bb.putDouble(24, d[3]);
		return bb.array();
	}
}
