import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;


public class DBaseReader extends Reader {
	int numRecords;
	int recordSize;
	int numColumns;
	String[] columnNames;
	char[] columnTypes;
	short[] fieldLengths;
	short[] decimalCount;

	public DBaseReader(File f) throws FileNotFoundException {
		super(f);
	}
	
	private int readInt16LittleEndian(byte[] bytes, int i){
		ByteBuffer bb = ByteBuffer.allocate(2);
		bb.order(ByteOrder.LITTLE_ENDIAN);
		bb.put(bytes, i, 2);
		bb.position(0);
		return bb.getShort();
	}
	
	private double readDoubleNumericLittleEndian(byte[] bytes, int i, int len){
		String number = readStringLittleEndian(bytes, i, len).trim();
		if (number.length() > 0) return Double.parseDouble(number);
		else return 0;
	}
	
	private long readLongNumericLittleEndian(byte[] bytes, int i, int len){
		String number = readStringLittleEndian(bytes, i, len).trim();
		if (number.length() > 0) return Long.parseLong(number);
		else return 0;
	}
	
	private float readFloatNumericLittleEndian(byte[] bytes, int i, int len){
		String number = readStringLittleEndian(bytes, i, len).trim();
		if (number.length() > 0) return Float.parseFloat(number);
		else return 0;
	}
	
	private int readIntNumericLittleEndian(byte[] bytes, int i, int len){
		String number = readStringLittleEndian(bytes, i, len).trim();
		if (number.length() > 0) return Integer.parseInt(number);
		else return 0;
	}
	
	private Number readNumberLittleEndian(byte[] bytes, int i, int len){
		String number = readStringLittleEndian(bytes, i, len).trim();
		if (number.length() > 0) {
			if (number.length() > 10) {
				if (number.contains(".")) return Double.parseDouble(number);
				else return Long.parseLong(number);
			} else {
				if (number.contains(".")) return Float.parseFloat(number);
				else return Integer.parseInt(number);
			}
		}
		else return 0;
	}
	
	private boolean readLogicalLittleEndian(byte[] bytes, int i){
		char bool = (char) bytes[i];
		switch(bool){
		case 'T': return true;
		case 'F': return false;
		default: return false;
		}
	}
	
	private String readStringLittleEndian(byte[] bytes, int i, int length){
		ByteBuffer bb = ByteBuffer.allocate(length);
		bb.order(ByteOrder.LITTLE_ENDIAN);
		bb.put(bytes, i, length);
		bb.position(0);
		return new String(bb.array());
	}

	private void readHeader() throws IOException{
		int headerLength = 32;
		byte[] header = new byte[headerLength];
		if (fis.read(header) == headerLength){
			numRecords = readIntLittleEndian(header, 4);
			int numBytes = readInt16LittleEndian(header, 8);
			recordSize = readInt16LittleEndian(header, 10);
			int columnsLength = numBytes - headerLength - 1;
			byte[] bytes = new byte[columnsLength];
			numColumns = columnsLength/32;
			System.out.println("numRecords: "+numRecords+", recordSize: "+recordSize);
			if (fis.read(bytes) == columnsLength){
				columnNames = new String[numColumns];
				columnTypes = new char[numColumns];
				fieldLengths= new short[numColumns];
				decimalCount= new short[numColumns];
				String coma = "FieldNames: ";
				for (int i = 0, j = 0; i < columnsLength; i += 32, j++){
					columnNames[j]  = readStringLittleEndian(bytes, i, 11);
					columnTypes[j]  = (char)  bytes[i + 11];
					fieldLengths[j] = (short) bytes[i + 16];
					decimalCount[j] = (short) bytes[i + 17];
					System.out.print(coma + columnNames[j] + ":" + columnTypes[j] + ":" + fieldLengths[j] + ":" + decimalCount[j]);
					coma = ", ";
				}
				System.out.println("");
				fis.read(new byte[1]); // terminator
			}
		}
	}
	
	public void read() throws IOException{
		readHeader();
		for (int i = 0; i < numRecords; i++){
			byte[] record = new byte[recordSize];
			if (fis.read(record) == recordSize){
				int n = 1;
				byte flag = record[0];
				for (int j = 0; j < numColumns; j++){
					switch(columnTypes[j]){
					case 'B': break; //System.out.println("B"); break;
					case 'C': System.out.print(columnNames[j] + ": " + readStringLittleEndian(record, n, fieldLengths[j]).trim() + ", "); break;
					case 'D': break; //System.out.println("D"); break;
					case 'N': System.out.print(columnNames[j] + ": " + readNumberLittleEndian(record, n, fieldLengths[j]) + ", "); break;
					case 'L': System.out.print(columnNames[j] + ": " + readLogicalLittleEndian(record, n) + ", ");break;
					case 'M': break; //System.out.println("M"); break;
					case 'F': System.out.print(columnNames[j] + ": " + readFloatNumericLittleEndian(record, n, fieldLengths[j])); break;
					case 'O': System.out.print(columnNames[j] + ": " + readDoubleLittleEndian(record, n) + ", "); break;
					default: System.out.println(columnTypes[j]);
					}
					n += fieldLengths[j];
				}
				System.out.println("");
			}
		}
	}
	
}
