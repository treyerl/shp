import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;


public class ShapefileReader extends Reader {
	private int filelength;
	private int shapetype;
	private double[] bbox;
	private final int fileHeaderSize = 50;
	
	public ShapefileReader(File f) throws FileNotFoundException {
		super(f);
		bbox = new double[8];
	}
	
	/**Method to split the points into parts
	 * @param partStartIdx
	 * @param points
	 * @return a 3-dimensional array holding the parts with its x,y(,z,m) points
	 */
	public double[][][] split(int[] partStartIdx, double[][] points){
		assert partStartIdx.length > 0;
		assert points.length > 0;
		int pl = partStartIdx.length;
		double[][][] parts = new double[pl][][];
		int[] partEndIdx = new int[pl];
		for (int i = 1; i < pl; i++){
			partEndIdx[i-1] = partStartIdx[i];
		}
		int pointlen = points[0].length;
		partEndIdx[pl-1] = points.length;
		int lasti = 0;
		int n = 0;
		for (int i: partEndIdx){
			double[][] part = new double[i - lasti][pointlen];
			for (int j = lasti, k = 0; j < i; j++, k++){
				for (int l = 0; l < pointlen; l++){
					part[k][l] = points[j][l];
				}
			}
			parts[n++] = part;
			lasti = i;
		}
		return parts;
	}

	private void point() throws IOException{
		int length = 0;
		while (length < filelength - fileHeaderSize){
			byte[] recordHeader = new byte[8];
			if (fis.read(recordHeader) == 8){
				int recordNumber = readIntBigEndian(recordHeader, 0);
				int contentLength = readIntBigEndian(recordHeader, 4);
				byte[] point = new byte[contentLength*2];
				if (fis.read(point) == contentLength * 2){
//					int shapetype = readIntLittleEndian(point, 0);
//					assert shapetype == Point;
					double x = readDoubleLittleEndian(point, 4);
					double y = readDoubleLittleEndian(point, 12);
					// TODO: define your actions for point
					System.out.println(recordNumber+"|"+contentLength+": "+x+"/"+y+"  "+length+":"+(filelength-50));
					length += contentLength + 4;
				}
			}
		}
	}
	
	private void multipoint() throws IOException{
		int length = 0;
		while(length < filelength - fileHeaderSize){
			byte[] recordHeader = new byte[8];
			if (fis.read(recordHeader) == 8){
				int recordNumber = readIntBigEndian(recordHeader, 0);
				int contentLength = readIntBigEndian(recordHeader, 4);
				byte[] multipoint = new byte[contentLength*2];
				if (fis.read(multipoint) == contentLength * 2){
//					int shapetype = readIntLittleEndian(point, 0);
//					assert shapetype == MultiPoint;
					double[] box = readBBoxLittleEndian(multipoint, 4);
					int numPoints = readIntLittleEndian(multipoint, 36);
					double[][] points = new double[numPoints][2];
					for (int i = 0; i < numPoints ; i++){
						int j = 40 + i * 20;
						points[i][0] = readDoubleLittleEndian(multipoint, j + 4);
						points[i][1] = readDoubleLittleEndian(multipoint, j + 12);
					}
					// TODO: define your actions for point
					System.out.println(recordNumber+"|"+contentLength+": from "+box[0]+"/"+box[1]+" to "+box[2]+"/"+box[3]+" containing "+numPoints+" points -- "+length+":"+(filelength-50));
					length += contentLength + 4;
				}
			}
		}
	}
	
	private double[][][] poly2d(byte[] bytes, int contentLength, double[] bbox){
//		int shapetype = readIntLittleEndian(point, 0);
//		assert shapetype == PolyLine;
		readBBoxLittleEndian(bytes, 4, bbox);
		int numParts = readIntLittleEndian(bytes, 36);
		int numPoints = readIntLittleEndian(bytes, 40);
		int[] parts = new int[numParts];
		double[][] points = new double[numPoints][2];
		for (int i = 44, j = 0; j < numParts; i += 4, j++){
			parts[j] = readIntLittleEndian(bytes, i);
		}
		for (int i = 44 + 4 * numParts, j = 0; j < numPoints; i += 16, j++){
			points[j][0] = readDoubleLittleEndian(bytes, i + 0);
			points[j][1] = readDoubleLittleEndian(bytes, i + 8);
		}
		return split(parts, points);
	}
	
	private void polyline() throws IOException{
		int length = 0;
		int n = 0;
		while(length < filelength - fileHeaderSize){
			byte[] recordHeader = new byte[8];
			if (fis.read(recordHeader) == 8){
				int recordNumber = readIntBigEndian(recordHeader, 0);
				int contentLength = readIntBigEndian(recordHeader, 4);
				byte[] bytes = new byte[contentLength*2];
				if (fis.read(bytes) == contentLength * 2){
					double[] bbox = new double[4];
					double[][][] polyline = poly2d(bytes, contentLength, bbox);
					int numPoints = 0;
					for (double[][] part: polyline){
						numPoints += part.length;
					}
					// TODO: define your actions for polyline
					System.out.println(recordNumber+"|"+contentLength+": from "+bbox[0]+"/"+bbox[1]+" to "+bbox[2]+"/"+bbox[3]+" containing "+polyline.length+" parts and "+numPoints+" points -- "+length+":"+(filelength-50));
					length += contentLength + 4;
					n++;
				}
			}
		}
		System.out.println(n + " polylines");
	}
	
	private void polygon() throws IOException{
		int length = 0;
		int n = 0;
		while(length < filelength - fileHeaderSize){
			byte[] recordHeader = new byte[8];
			if (fis.read(recordHeader) == 8){
				int recordNumber = readIntBigEndian(recordHeader, 0);
				int contentLength = readIntBigEndian(recordHeader, 4);
				byte[] bytes = new byte[contentLength*2];
				if (fis.read(bytes) == contentLength * 2){
					double[] bbox = new double[4];
					double[][][] polygon = poly2d(bytes, contentLength, bbox);
					for (double[][] part: polygon){
						int len = part.length;
						assert part[0][0] == part[len][0];
						assert part[0][1] == part[len][1];
					}
					// TODO: define your actions for polyline
					//System.out.println(recordNumber+"|"+contentLength+": from "+bbox[0]+"/"+bbox[1]+" to "+bbox[2]+"/"+bbox[3]+" containing "+numParts+" parts and "+numPoints+" points -- "+length+":"+(filelength-50));
					length += contentLength + 4;
					n++;
				}
			}
		}
		System.out.println(n + " polygons");
	}
	
	private void pointz() throws IOException{
		int length = 0;
		while (length < filelength - fileHeaderSize){
			byte[] recordHeader = new byte[8];
			if (fis.read(recordHeader) == 8){
				int recordNumber = readIntBigEndian(recordHeader, 0);
				int contentLength = readIntBigEndian(recordHeader, 4);
				byte[] pointz = new byte[contentLength*2];
				if (fis.read(pointz) == contentLength * 2){
//					int shapetype = readIntLittleEndian(point, 0);
//					assert shapetype == Point;
					double x = readDoubleLittleEndian(pointz, 4);
					double y = readDoubleLittleEndian(pointz, 12);
					double z = readDoubleLittleEndian(pointz, 20);
					double m = readDoubleLittleEndian(pointz, 28);
					// TODO: define your actions for point
					System.out.println(recordNumber+"|"+contentLength+": "+x+"/"+y+"/"+z+"-"+m+"  "+length+":"+(filelength-50));
					length += contentLength + 4;
				}
			}
		}
	}
	
	private void multipointz() throws IOException{
		int length = 0;
		int n = 0;
		while (length < filelength - fileHeaderSize){
			byte[] recordHeader = new byte[8];
			if (fis.read(recordHeader) == 8){
				int recordNumber = readIntBigEndian(recordHeader, 0);
				int contentLength = readIntBigEndian(recordHeader, 4);
				byte[] bytes = new byte[contentLength*2];
				if (fis.read(bytes) == contentLength * 2){
//					int shapetype = readIntLittleEndian(point, 0);
//					assert shapetype == Point;
					double[] box = readBBoxLittleEndian(bytes, 4);
					int numPoints = readIntLittleEndian(bytes, 36);
					int X = 40 + 16 * numPoints;
					int Y = X + 16 + 8 * numPoints;
					boolean hasM = Y + 16 + 8 * numPoints == contentLength;
					double[][] points;
					if (hasM) points = new double[numPoints][4];
					else points = new double[numPoints][3];
					for (int i = 40, j = 0; j < numPoints; i += 16, j++) {
						points[j][0] = readDoubleLittleEndian(bytes, i);
						points[j][1] = readDoubleLittleEndian(bytes, i + 8);
					}
					double zmin = readDoubleLittleEndian(bytes, X);
					double zmax = readDoubleLittleEndian(bytes, X + 8);
					for (int i = X + 16, j = 0; j < numPoints; i += 8, j++){
						points[j][2] = readDoubleLittleEndian(bytes, i);
					}
					if (hasM){
						double mmin = readDoubleLittleEndian(bytes, Y);
						double mmax = readDoubleLittleEndian(bytes, Y + 8);
						for (int i = Y + 16, j = 0; j < numPoints; i += 8, j++){
							points[j][3] = readDoubleLittleEndian(bytes, i);
						}
					}
					// TODO: define your actions for point
					//System.out.println(recordNumber+"|"+contentLength+": "+x+"/"+y+"/"+z+"-"+m+"  "+length+":"+(filelength-50));
					length += contentLength + 4;
					n++;
				}
			}
		}
		System.out.println(n + " multipointZ");
	}
	
	private double[][][] poly3d(byte[] bytes, int contentLength, double[] bbox){
//		int shapetype = readIntLittleEndian(point, 0);
//		assert shapetype == PolyLine;
		readBBoxLittleEndian(bytes, 4, bbox);
		int numParts = readIntLittleEndian(bytes, 36);
		int numPoints = readIntLittleEndian(bytes, 40);
		int X = 44 + 4 * numParts; 		// start of points
		int Y = X + 16 * numPoints;		// start of z values
		int Z = Y + 16 + 8 * numPoints; // start of m values
		// since m measures are optional we test if there are more bytes available
		boolean hasM = Z + 16 + 8 * numPoints == contentLength;
		int[] parts = new int[numParts];
		double[][] points;
		if (hasM) points = new double[numPoints][4];
		else points = new double[numPoints][3];
		
		// parts
		for (int i = 44, j = 0; j < numParts; i += 4, j++){
			parts[j] = readIntLittleEndian(bytes, i);
		}
		
		// points
		for (int i = X, j = 0; j < numPoints; i += 16, j++){
			points[j][0] = readDoubleLittleEndian(bytes, i + 0);
			points[j][1] = readDoubleLittleEndian(bytes, i + 8);
		}
		
		// z values
		double zmin = readDoubleLittleEndian(bytes, Y);
		double zmax = readDoubleLittleEndian(bytes, Y + 8);
		for (int i = Y + 16, j = 0; j < numPoints; i += 8, j++){
			points[j][2] = readDoubleLittleEndian(bytes, i); 
		}
		
		// m values
		if (hasM){
			double mmin = readDoubleLittleEndian(bytes, Z);
			double mmax = readDoubleLittleEndian(bytes, Z + 8);
			for (int i = Z + 16, j = 0; j < numPoints; i += 8, j++){
				points[j][3] = readDoubleLittleEndian(bytes, i);
			}
		}
		return split(parts, points);
	}
	
	private void polylinez() throws IOException{
		int length = 0;
		int n = 0;
		while(length < filelength - fileHeaderSize){
			byte[] recordHeader = new byte[8];
			if (fis.read(recordHeader) == 8){
				int recordNumber = readIntBigEndian(recordHeader, 0);
				int contentLength = readIntBigEndian(recordHeader, 4);
				byte[] bytes = new byte[contentLength*2];
				if (fis.read(bytes) == contentLength * 2){
					double[] bbox = new double[4];
					double[][][] polylinez = poly3d(bytes, contentLength, bbox);
					
					// TODO: define your actions for polylinez
					//System.out.println(recordNumber+"|"+contentLength+": from "+bbox[0]+"/"+bbox[1]+" to "+bbox[2]+"/"+bbox[3]+" containing "+numParts+" parts and "+numPoints+" points -- "+length+":"+(filelength-50));
					length += contentLength + 4;
					n++;
				}
			}
		}
		System.out.println(n + " polylineZ");
	}
	private void polygonz() throws IOException{
		int length = 0;
		int n = 0;
		while(length < filelength - fileHeaderSize){
			byte[] recordHeader = new byte[8];
			if (fis.read(recordHeader) == 8){
				int recordNumber = readIntBigEndian(recordHeader, 0);
				int contentLength = readIntBigEndian(recordHeader, 4);
				byte[] bytes = new byte[contentLength*2];
				if (fis.read(bytes) == contentLength * 2){
					double[] bbox = new double[4];
					double[][][] polygonz = poly3d(bytes, contentLength, bbox);
					for (double[][] part: polygonz){
						int len = part.length;
						assert part[0][0] == part[len][0];
						assert part[0][1] == part[len][1];
						assert part[0][2] == part[len][2];
					}
					// TODO: define your actions for polylinez
					//System.out.println(recordNumber+"|"+contentLength+": from "+bbox[0]+"/"+bbox[1]+" to "+bbox[2]+"/"+bbox[3]+" containing "+numParts+" parts and "+numPoints+" points -- "+length+":"+(filelength-50));
					length += contentLength + 4;
					n++;
				}
			}
		}
		System.out.println(n + " polygonZ");
	}
	
	private void pointm() throws IOException{
		int length = 0;
		while (length < filelength - fileHeaderSize){
			byte[] recordHeader = new byte[8];
			if (fis.read(recordHeader) == 8){
				int recordNumber = readIntBigEndian(recordHeader, 0);
				int contentLength = readIntBigEndian(recordHeader, 4);
				byte[] pointz = new byte[contentLength*2];
				if (fis.read(pointz) == contentLength * 2){
//					int shapetype = readIntLittleEndian(point, 0);
//					assert shapetype == Point;
					double x = readDoubleLittleEndian(pointz, 4);
					double y = readDoubleLittleEndian(pointz, 12);
					double m = readDoubleLittleEndian(pointz, 20);
					// TODO: define your actions for point
					System.out.println(recordNumber+"|"+contentLength+": "+x+"/"+y+"-"+m+"  "+length+":"+(filelength-50));
					length += contentLength + 4;
				}
			}
		}
	}
	
	private void multipointm() throws IOException{
		int length = 0;
		int n = 0;
		while (length < filelength - fileHeaderSize){
			byte[] recordHeader = new byte[8];
			if (fis.read(recordHeader) == 8){
				int recordNumber = readIntBigEndian(recordHeader, 0);
				int contentLength = readIntBigEndian(recordHeader, 4);
				byte[] bytes = new byte[contentLength*2];
				if (fis.read(bytes) == contentLength * 2){
//					int shapetype = readIntLittleEndian(point, 0);
//					assert shapetype == Point;
					double[] box = readBBoxLittleEndian(bytes, 4);
					int numPoints = readIntLittleEndian(bytes, 36);
					int X = 40 + 16 * numPoints;
					double[][] points = new double[numPoints][3];
					for (int i = 40, j = 0; j < numPoints; i += 16, j++) {
						points[j][0] = readDoubleLittleEndian(bytes, i);
						points[j][1] = readDoubleLittleEndian(bytes, i + 8);
					}
					double mmin = readDoubleLittleEndian(bytes, X);
					double mmax = readDoubleLittleEndian(bytes, X + 8);
					for (int i = X + 16, j = 0; j < numPoints; i += 8, j++){
						points[j][2] = readDoubleLittleEndian(bytes, i);
					}
					
					// TODO: define your actions for point
					//System.out.println(recordNumber+"|"+contentLength+": "+x+"/"+y+"/"+z+"-"+m+"  "+length+":"+(filelength-50));
					length += contentLength + 4;
					n++;
				}
			}
		}
		System.out.println(n + " multipointZ");
	}

	private double[][][] poly2dM(byte[] bytes, int contentLength, double[] bbox){
//		int shapetype = readIntLittleEndian(point, 0);
//		assert shapetype == PolyLine;
		readBBoxLittleEndian(bytes, 4, bbox);
		int numParts = readIntLittleEndian(bytes, 36);
		int numPoints = readIntLittleEndian(bytes, 40);
		int X = 44 + 4 * numParts; 		// start of points
		int Y = X + 16 * numPoints;		// start of z values
		// since m measures are optional we test if there are more bytes available
		int[] parts = new int[numParts];
		double[][] points = new double[numPoints][3];
		
		// parts
		for (int i = 44, j = 0; j < numParts; i += 4, j++){
			parts[j] = readIntLittleEndian(bytes, i);
		}
		
		// points
		for (int i = X, j = 0; j < numPoints; i += 16, j++){
			points[j][0] = readDoubleLittleEndian(bytes, i + 0);
			points[j][1] = readDoubleLittleEndian(bytes, i + 8);
		}
		
		// m values
		double mmin = readDoubleLittleEndian(bytes, Y);
		double mmax = readDoubleLittleEndian(bytes, Y + 8);
		for (int i = Y + 16, j = 0; j < numPoints; i += 8, j++){
			points[j][2] = readDoubleLittleEndian(bytes, i); 
		}
		return split(parts, points);
	}
	
	private void polylinem() throws IOException{
		int length = 0;
		int n = 0;
		while(length < filelength - fileHeaderSize){
			byte[] recordHeader = new byte[8];
			if (fis.read(recordHeader) == 8){
				int recordNumber = readIntBigEndian(recordHeader, 0);
				int contentLength = readIntBigEndian(recordHeader, 4);
				byte[] bytes = new byte[contentLength*2];
				if (fis.read(bytes) == contentLength * 2){
					double[] bbox = new double[4];
					double[][][] polylinez = poly2dM(bytes, contentLength, bbox);
					
					// TODO: define your actions for polylinez
					//System.out.println(recordNumber+"|"+contentLength+": from "+bbox[0]+"/"+bbox[1]+" to "+bbox[2]+"/"+bbox[3]+" containing "+numParts+" parts and "+numPoints+" points -- "+length+":"+(filelength-50));
					length += contentLength + 4;
					n++;
				}
			}
		}
		System.out.println(n + " polylineM");
	}
	
	private void polygonm() throws IOException{
		int length = 0;
		int n = 0;
		while(length < filelength - fileHeaderSize){
			byte[] recordHeader = new byte[8];
			if (fis.read(recordHeader) == 8){
				int recordNumber = readIntBigEndian(recordHeader, 0);
				int contentLength = readIntBigEndian(recordHeader, 4);
				byte[] bytes = new byte[contentLength*2];
				if (fis.read(bytes) == contentLength * 2){
					double[] bbox = new double[4];
					double[][][] polygonm = poly3d(bytes, contentLength, bbox);
					for (double[][] part: polygonm){
						int len = part.length;
						assert part[0][0] == part[len][0];
						assert part[0][1] == part[len][1];
						assert part[0][2] == part[len][2];
					}
					// TODO: define your actions for polylinez
					//System.out.println(recordNumber+"|"+contentLength+": from "+bbox[0]+"/"+bbox[1]+" to "+bbox[2]+"/"+bbox[3]+" containing "+numParts+" parts and "+numPoints+" points -- "+length+":"+(filelength-50));
					length += contentLength + 4;
					n++;
				}
			}
		}
		System.out.println(n + " polygonM");
	}
	
	private void multipatch() throws IOException{
		int length = 0;
		int n = 0;
		while(length < filelength - fileHeaderSize){
			byte[] recordHeader = new byte[8];
			if (fis.read(recordHeader) == 8){
				int recordNumber = readIntBigEndian(recordHeader, 0);
				int contentLength = readIntBigEndian(recordHeader, 4);
				byte[] bytes = new byte[contentLength*2];
				if (fis.read(bytes) == contentLength * 2){
					double[] bbox = readBBoxLittleEndian(bytes, 4);
					int numParts = readIntLittleEndian(bytes, 36);
					int numPoints = readIntLittleEndian(bytes, 40);
					int W = 44 + 4 * numParts;
					int X = W + 4 * numParts;
					int Y = X + 16 * numPoints;
					int Z = Y + 16 + 8 * numPoints;
					int[] parts = new int[numParts];
					int[] partTypes = new int[numParts];
					boolean hasM = Z + 16 + 8 * numPoints == contentLength;
					double[][] points;
					if (hasM) points = new double[numPoints][4];
					else points = new double[numPoints][3];
					
					// parts
					for (int i = 44, j = 0; j < numParts; i += 4, j++){
						parts[j] = readIntLittleEndian(bytes, i);
					}
					
					// partTypes
					for (int i = W, j = 0; j < numParts; i += 4, j++){
						partTypes[j] = readIntLittleEndian(bytes, i);
					}
					
					// points
					for (int i = X, j = 0; j< numPoints; i += 16, j++){
						points[j][0] = readDoubleLittleEndian(bytes, i);
						points[j][1] = readDoubleLittleEndian(bytes, i + 8);
					}
					
					// z values
					double zmin = readDoubleLittleEndian(bytes, Y);
					double zmax = readDoubleLittleEndian(bytes, Y + 8);
					for (int i = Y + 16, j = 0; j < numPoints; i += 8, j++){
						points[j][2] = readDoubleLittleEndian(bytes, i);
					}
					
					// m values
					if (hasM){
						double mmin = readDoubleLittleEndian(bytes, Z);
						double mmax = readDoubleLittleEndian(bytes, Z + 8);
						for (int i = Z + 16, j = 0; j < numPoints; i += 8, j++){
							points[j][3] = readDoubleLittleEndian(bytes, i);
						}
					}
					// TODO: define your actions for multipatch
					//System.out.println(recordNumber+"|"+contentLength+": from "+bbox[0]+"/"+bbox[1]+" to "+bbox[2]+"/"+bbox[3]+" containing "+numParts+" parts and "+numPoints+" points -- "+length+":"+(filelength-50));
					length += contentLength + 4;
					n++;
				}
			}
		}
		System.out.println(n + " multipatch");
	}
	
	private void readType() throws IOException{
		switch(shapetype){
		case Point: point(); break;
		case PolyLine: polyline(); break;
		case Polygon: polygon(); break; 
		case MultiPoint: multipoint(); break;
		case PointZ: pointz(); break;
		case PolyLineZ: polylinez(); break;
		case PolygonZ: polygonz(); break;
		case MultiPointZ: multipointz(); break;
		case PointM: pointm(); break; 
		case PolyLineM: polylinem(); break; 
		case PolygonM: polygonm(); break;
		case MultiPointM: multipointm(); break;
		case MultiPatch: multipatch(); break;
		}
	}
	
	private String typeName(){
		switch(shapetype){
		case Point: return "Point";
		case PolyLine: return "PolyLine";
		case Polygon: return "Polygon"; 
		case MultiPoint: return "MultiPoint";
		case PointZ: return "PointZ";
		case PolyLineZ: return "PolyLineZ";
		case PolygonZ: return "PolygonZ";
		case MultiPointZ: return "MultiPointZ";
		case PointM: return "PointM"; 
		case PolyLineM: return "PolyLineM"; 
		case PolygonM: return "PolygonM";
		case MultiPointM: return "MultiPointM";
		case MultiPatch: return "MultiPatch";
		default: return "NULL";
		}
	}
	
	private void readHeader() throws IOException{
		byte[] header = new byte[100];
		if (fis.read(header) == 100){
			int filecode = readIntBigEndian(header, 0);
			filelength = readIntBigEndian(header, 24); // file length is in 16bit words
			int version = readIntLittleEndian(header, 28);
			shapetype = readIntLittleEndian(header, 32);
			bbox[0] = readDoubleLittleEndian(header, 36); // xmin 0
			bbox[1] = readDoubleLittleEndian(header, 44); // ymin 1
			bbox[2] = readDoubleLittleEndian(header, 52); // xmax 2
			bbox[3] = readDoubleLittleEndian(header, 60); // ymax 3
			bbox[4] = readDoubleLittleEndian(header, 68); // zmin 4
			bbox[5] = readDoubleLittleEndian(header, 76); // zmax 5
			bbox[6] = readDoubleLittleEndian(header, 84); // mmin 6
			bbox[7] = readDoubleLittleEndian(header, 92); // mmax 7
			System.out.println(filecode);
			System.out.println("filelength: " + filelength);
			System.out.println("version: " + version);
			System.out.println(typeName());
			System.out.println("xy["+bbox[0]+","+bbox[1]+"|"+bbox[2]+","+bbox[3]+"]");
			System.out.println("zm["+bbox[4]+","+bbox[5]+"|"+bbox[6]+","+bbox[7]+"]");
		}
	}

	public void read() throws IOException{
		readHeader();
		readType();
	}
}
