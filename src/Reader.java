import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;


public class Reader extends SHP{
	protected FileInputStream fis;
	
	public Reader(File f) throws FileNotFoundException {
		super(f);
		fis = new FileInputStream(file);
	}

	protected int readIntLittleEndian(byte[] b, int i){
		ByteBuffer bb = ByteBuffer.allocate(4);
		bb.order(ByteOrder.LITTLE_ENDIAN);
		bb.put(b, i, 4);
		bb.position(0);
		return bb.getInt();
	}
	
	protected int readIntBigEndian(byte[] b, int i){
		ByteBuffer bb = ByteBuffer.allocate(4);
		bb.put(b, i, 4);
		bb.position(0);
		return bb.getInt();
	}
	
	protected double readDoubleLittleEndian(byte[] b, int i){
		ByteBuffer bb = ByteBuffer.allocate(8);
		bb.order(ByteOrder.LITTLE_ENDIAN);
		bb.put(b, i, 8);
		bb.position(0);
		return bb.getDouble();
	}
	
	protected double[] readBBoxLittleEndian(byte[] b, int i){
		double[] bbox = new double[4];
		readBBoxLittleEndian(b, i, bbox);
		return bbox;
	}
	
	protected void readBBoxLittleEndian(byte[] b, int i, double[] bbox){
		assert bbox.length == 4;
		bbox[0] = readDoubleLittleEndian(b, i);
		bbox[1] = readDoubleLittleEndian(b, i + 8);
		bbox[2] = readDoubleLittleEndian(b, i + 16);
		bbox[3] = readDoubleLittleEndian(b, i + 24);
	}
	
	protected String readHex(byte[] b, int i, int len){
		StringBuilder sb = new StringBuilder(len);
		for (int j = 0; j < len; j++){
			sb.append(String.format("%x", b[i + j] & 0xff));
		}
		return sb.toString();
	}
}
