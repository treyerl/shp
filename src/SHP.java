import java.io.File;
import java.io.IOException;


public class SHP {
	public final int NullShape = 0;
	public final int Point = 1;
	public final int PolyLine = 3;
	public final int Polygon = 5; 
	public final int MultiPoint = 8;
	public final int PointZ = 11;
	public final int PolyLineZ = 13;
	public final int PolygonZ = 15;
	public final int MultiPointZ = 18;
	public final int PointM = 21; 
	public final int PolyLineM = 23; 
	public final int PolygonM = 25;
	public final int MultiPointM = 28;
	public final int MultiPatch = 31;
	
	File file;
	
	public SHP(File f){
		file = f;
	}

	public static void main(String[] args) throws IOException{
		ShapefileReader points = new ShapefileReader(
				new File("/Users/treyerl/Developer/bitbucket/lucy/data/VEC25_anl_p_Clip/VEC25_anl_p.shp"));
		DBaseReader pointsDBF = new DBaseReader(
				new File("/Users/treyerl/Developer/bitbucket/lucy/data/VEC25_anl_p_Clip/VEC25_anl_p.dbf"));
		ShapefileReader lines = new ShapefileReader(
				new File("/Users/treyerl/Developer/bitbucket/lucy/data/VEC25_eis_l_Clip/VEC25_eis_l.shp"));
		DBaseReader linesDBF = new DBaseReader(
				new File("/Users/treyerl/Developer/bitbucket/lucy/data/VEC25_eis_l_Clip/VEC25_eis_l.dbf"));
		ShapefileReader polygons = new ShapefileReader(
				new File("/Users/treyerl/Developer/bitbucket/lucy/data/VEC25_geb_a_Clip/VEC25_geb_a.shp"));
		DBaseReader polygonsDBF = new DBaseReader(
				new File("/Users/treyerl/Developer/bitbucket/lucy/data/VEC25_geb_a_Clip/VEC25_geb_a.dbf"));
		//points.read();
		//pointsDBF.read();
		lines.read();
		linesDBF.read();
		//polygons.read();
		//polygonsDBF.read();
	}

}
