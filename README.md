# SHP #

Five classes intended as a template for shapefile & dbasefile reading. "Template" means: the only thing it does is printing the information stored in .shp and .dbf files on the console; feel free to add println statements. Nevertheless it is you, the programmer applying this template, deciding how to further process this information.

### Class hierarchy ###

* SHP.java  - holding constants and static void main(String[] args)
* * Reader.java - some "read big and little endian" methods
* * * ShapefileReader.java - reading file header and file contents
* * * DBasefileReader.java - reading file header and file contents
* * Writer.java - some "write big and little endian" methods


### How do I get set up? ###

* You need a few shapefiles; I might add some links here later.
* Create a java project in your favorite Java developer environment.
* Change the paths in the SHP class (in the main method).